# Infraestrutura para Ambiente de Teste com Docker #

![docker](https://cdn.iconscout.com/icon/free/png-256/docker-226091.png)

## Para que serve este repositório? ##

Este repositório tem como objetivo básico auxiliar a execução do micro serviço `product-ms`. Esta configuração docker, está preparada para executar multiplos containeres que atendem os requisitos mínimos de infraestrutura exigidas pela aplicação desenvolvida.

### Imagens Utilizadas ###

- [OpenJDK](https://hub.docker.com/_/openjdk)
- [MongoDB](https://hub.docker.com/_/mongo)
- [MongoExpress](https://hub.docker.com/_/mongo-express)

## Como preparar o meu ambiente?  ##

### Pré-requisitos

1 - Cliente Git

- Primeiramente será necessário possuir um cliente `GIT` instalado na máquina host. Caso não possua, acesse [Instalando o Git](https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git) e siga as instruções de instalação.

2 - Docker

- Necessário possuir o `Docker Engine` na versão `18.06.0+`
- Caso não esteja instalado, acesse [Install Docker Engine](https://docs.docker.com/engine/install/), selecione a plataforma correta e prossiga com a instalação.
- **IMPORTANTE**: Observar a versão especificada acima, para compatibiliade com o `Compose file format` do arquivo de configuração da ferramenta `docker-compose`.

3 - Dependências

- Verifique se o `docker-compose` está disponível:
    * Abra o terminal e execute o comando
```
    $ docker-compose -v 
    docker-compose version 1.27.4, build 40524192
```

### Iniciar o Setup do ambiente ###

Depois de verificar os pré-requisitos necessários, vamos iniciar as configurações!

1 - Faça um clone deste repositório em qualquer diretório:

- No terminal, acesse o diretório selecionado para o clone. Exemplo:
```
    $ cd compassouol_gupy_60550850/

```
- Com a `URL` do repositório, faça o clone:
```
    $ git clone https://bitbucket.org/murilo8812/docker_ms_product.git
```

- **IMPORTANTE**: Após o clone, Certifique-se de que a branch ativa é a `master`:
```
    $ git branch
    $ * master
```
* A branch `master` sempre contém a versão mais recente dos artefatos de build do projeto.

2 - Inicializar os containeres com o `docker-compose`

- No terminal, acesse o diretório `root` do projeto `docker_ms_product` e execute o comando abaixo:
```
    $ docker-compose up -d --build --force-recreate
```
- Aguarde o processo de download das imagens necessárias. Este processo pode demorar alguns minutos na primeira execução.
- Certifique-se de que todos os containeres estão em execução
```
    $ docker ps -a \
    CONTAINER ID   IMAGE                  COMMAND                  CREATED          STATUS                  PORTS                      NAMES
    dbc6b83d5de0   openjdk_spring_boot    "/bin/sh -c 'java ${…"   49 minutes ago   Up 49 minutes           0.0.0.0:9999->8080/tcp     docker_ms_product_application-server_1
    9154c302b6a1   mongo-express:latest   "tini -- /docker-ent…"   49 minutes ago   Up 49 minutes           0.0.0.0:9998->8081/tcp     docker_ms_product_mongo-express_1
    7aed581cc807   mongo:latest           "docker-entrypoint.s…"   49 minutes ago   Up 49 minutes           0.0.0.0:27017->27017/tcp   docker_ms_product_mongodb_1
    $
```

### Execução dos serviços disponíveis ###

- **docker_ms_product_application-server**
    * Utiliza imagem customizada a partir da imagem base `openjdk`
    * Disponibiliza servidor de aplicação embedded a partir da execução da aplicação `SpringBoot`
        - Micro Serviço `product-ms`
            * [API Rest](http://localhost:9999/products) disponível na porta default `TCP 9999`
            * [Documentação da API](http://localhost:9999/swagger-ui.html)
- **docker_ms_product_mongodb**
    * Disponibiliza servidor para persistência de Dados NoSQL orientado a documentos
    * Porta defaul `TCP 27017`
- **docker_ms_product_mongo-express**
    * Disponibiliza [ferramenta web](http://localhost:9998/) para gerenciamento da base de dados MongoDB
    * Porta defaul `TCP 9998`

**Observações**
- A configuração docker-compose inclui um arquivo de variáveis de ambiente `.env`, no diretório `root` do projeto `docker_ms_product`, que são utilizadas durante o build das imagens e configuração dos containeres. Todas as variáveis já estão pré-configuradas com os valores padrão e não é necessário nenhum ajuste. Porém, caso seja necessário, edite o arquivo e altere os valores conforme suas necessidades.

## Soluções de Problemas

`Bind for 0.0.0.0:<port>: unexpected error Permission denied`

**Causa:**

A porta especificada do host já está em uso por outra aplicação.

**Solução:**

Abra o arquivo `.env` e ajuste o mapeamento da porta especificada. Você deverá definir uma porta que não está em utilização por outra aplicação.
